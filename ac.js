
var AC = AC || { locks: {} };

/**
 * Send an ajax request.
 *
 * @param string module
 *   Module name
 *
 * @param string op
 *   Operation
 *
 * @param object data
 *   (optional) Key:value pairs used to generate query string.
 *   When omitted this will be considered the callback function.
 *
 * @param function callback
 *   Callback function once request is complete.
 */
AC.request = function(module, op, data, callback) { 
  var d = typeof data == 'function' ? {} : data;
  var c = typeof data == 'function' ? data : callback;
  var u = Drupal.settings.basePath + 'js/' + module + '/' + op;
          
  if (!AC.isLocked(module, op)){
    d.token = Drupal.settings.ac.token;
    $.post(u, d, function(response){
      c(response);
    }, 'json');
  }
};

/**
 * Validate response.
 *
 * @param object response
 *
 * @return bool
 */
AC.checkResponse = function(response) {
  var s = false; 

  // Check for lock
  if (response.lock){
	  AC.setLock(response._module, response._op, response.lock, response.lock_message, response.message_wrapper, response.message_class, response.message_position);
  }
  
  // Check debugging js         
  if (response.debug) {
    eval(response.debug);
  }
  // Error
  else if (!response.status){
    AC.setMessage(response.message_wrapper, response.message, 'error', response.message_duration, response.message_class, response.message_position);  
  }
  // Warning 
  else if (response.status == 2){
	  if (response.message.length){
	    AC.setMessage(response.message_wrapper, response.message, 'warning', response.message_duration, response.message_class, response.message_position);
	  }
	  s = true;
  }
  // Success
  else {   
    if (response.message.length){
      AC.setMessage(response.message_wrapper, response.message, 'status', response.message_duration, response.message_class, response.message_position);  
    }
    s = true;
  }
  
  return s;
};

/**
 * Set message.
 *
 * @param string wrapper
 *
 * @param string message
 *
 * @param string type
 *   (optional) Defaults to 'status'.
 *
 * @param int duration
 *   (optional) Message duration. Defaults to 3000;
 *
 * @param string classes
 *   (optional) Additional classes for this message.
 * 
 * @param string position
 *   (optional) This position string is evaluted as JavaScript allowing 
 *   you to optionally traverse and alter where the messages should be  
 *   displayed relative to the wrapper. Ex: '.parent().after(message)'
 */
AC.setMessage = function(wrapper, message, type, duration, classes, position) {
  var d = duration == null ? 3000 : duration; 
  var t = type || 'status';
  var c = classes || '';
  var p = position || null;
  message = '<div class="messages ac-message ac-message-' + t + ' ' + t + ' ' + c + '">' + message + '</div>';
        
  if (p){ 
	  eval('$(\'' + wrapper + '\')' + p + ';');
  }
  else {
	  $(wrapper).html(message);
  }                   
  
  if (d){        
    setTimeout('AC.clearMessages()', d);
  }
};

/**
 * Lock a request.
 *
 * @param string module
 *
 * @param string op
 * 
 * @param int duration
 *   Duration in seconds to lock this request.
 *
 * @param string message
 *   (optional) Lock message.
 * 
 * @param string classes
 *   (optional) Additional classes for this message.
 * 
 * @param string position
 *   (optional) This position string is evaluted as JavaScript allowing 
 *   you to optionally traverse and alter where the messages should be  
 *   displayed relative to the wrapper. Ex: '.parent().after(message)'
 */
AC.setLock = function(module, op, duration, message, wrapper, classes, position) {
	// @todo: accept wrapper and duration
	var d = duration * 1000;
	var m = message || '';
	var w = wrapper || '#ac-messages';
	var date = new Date();
	AC.locks[module] = {}; 
	AC.locks[module][op] = { 
		  duration: date.getTime() + d, 
		  message: m,
		  wrapper: w,
		  classes: classes,
		  position: position
		};
};

/**
 * Check a request lock.
 *
 * @param string module
 *
 * @param string op
 * 
 * @return bool
 *  Weither or not the request is locked.
 */
AC.isLocked = function(module, op) {
	if (AC.locks[module] && AC.locks[module][op]){ 
	  var date = new Date(); 
		if (AC.locks[module][op].duration <= date.getTime()){
		  delete AC.locks[module][op]; 
		}
		else {  
			// @todo: Display lock_message using message_wrapper and message_duration, display as notification with own class
			AC.setMessage(AC.locks[module][op].wrapper, AC.locks[module][op].message, 'warning', 3000, 'ac-lock ' + AC.locks[module][op].classes, AC.locks[module][op].position);
			return true;
		}
	} 
	
	return false;
};

/**
 * Clear messages set by AC.
 */
AC.clearMessages = function() {
  $('.ac-message').fadeOut(600);
};
