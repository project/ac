       Ajax Controller
       Developed by Tj Holowaychuk
       Maintained by AC
       
       ------------------------------------------------------------------------------- 
       INSTALLATION
       ------------------------------------------------------------------------------- 
       
       Simple enable the module. 
     
       ------------------------------------------------------------------------------- 
       PERMISSIONS
       ------------------------------------------------------------------------------- 
       
       None 
         
       ------------------------------------------------------------------------------- 
       PUBLIC API
       -------------------------------------------------------------------------------
       
       AC.request();
       AC.checkResponse();   
       ac_debug();   
        
       ------------------------------------------------------------------------------- 
       HOOKS
       -------------------------------------------------------------------------------
          
       - hook_js_response_alter(&$response, $module, $op) 
         Allows modules to alter ajax callback responses before they are outputted
         to the client or browser. This is useful for tasks such as changing the 
         message output wrapper for all responses.
         
       ------------------------------------------------------------------------------- 
       USAGE EXAMPLE
       -------------------------------------------------------------------------------
       
       // example.js      
       AC.request('example', 'delete', { 'content_id' : 1 }, function(response){
          if (AC.checkResponse(response)){
            $('#content-1').remove();
          }
       });  
       
       // example.module
       function example_js_delete(&$state, $args) {
         $state['message_wrapper'] = '#content #message-wrapper';
         
         if ($cid = $args['content_id']){  
           if (example_delete($cid)){
             $state['message'] = t('Request complete');
           }
           else {
             $state['status'] = AC_STATUS_ERROR;
             $state['message'] = t('Failed to delete !cid.', array('!cid' => $cid));
           }
         }

         // Always lock request for 5 seconds regardless of errors
         $state['lock'] = 5;
       }
       
       // Default response members
       // see ac_get_default_response() in ac.inc for details
       status = AC_STATUS_OK
       message = '' 
       message_wrapper = '#ac-messages' 
       message_duration = 3000 
       message_position = NULL
       message_class = ''
       output = NULL 
       encoding = AC_ENCODING_AUTO 
       format = AC_OUTPUT_JSON 
       lock = 0
       headers = array() 

       ------------------------------------------------------------------------------- 
       FUTURE GOALS
       ------------------------------------------------------------------------------- 
       
       - Add hook_ac similar to menu hook instead, which would allow
         for the cache mechanism to work correctly, and store callbacks in includes
         however this would be slight overkill for most implementations. 
         Consider extending menu hook although unlikely.
       - Add caching flags similar to the block module
       - Add message effects
       - Add 'message_func' allowing jQuery to prepend, append, replace, or insert messages
         into the DOM rather than JUST inserting.
       
           
 
 

