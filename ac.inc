<?php

/**
 * @file 
 * Ajax callback controller API.
 * @author Tj Holowaychuk <tj@vision-media.ca>
 * @maintainer ac <alex@spoon.com.au>
 * @package ac
 */
 
/**
 * Process the AC callback.
 * 
 * @param string $module
 *   Should be either a valid module name or active theme.
 * 
 * @param string $op
 *
 * @todo: check if a specific theme exists
 */
function ac_process_request($module, $op) {
  $func = "{$module}_js_{$op}";

  // If the module does not exist, try to see if it
  // matches with a theme name.
  if (! module_exists($module)) {
    // If theme has already been initialized, it is too late: init_theme() function does
    // not handle theme override.
    if (! isset($GLOBALS['theme'])) {
      $themes = list_themes();
    
      if (array_key_exists($module, $themes)) {
        global $custom_theme;
        $custom_theme = $module;
        init_theme();
      }
    }
  }

  // Ensure callback exists
  if (!function_exists($func)){
    watchdog('ac', 'Invalid callback function @function', array('@function' => $func), WATCHDOG_NOTICE);
    die(t('Invalid callback.'));
  }
  
  // Validate token
  if (!isset($_POST['token']) || !_ac_check_token($_POST['token'])){
    watchdog('ac', 'Invalid token');
    die(t('Callback failed.'));
  }
  
  // Build args from query string
  $args = $_POST;
  unset($args['q']);   

  // Execute callback   
  $response = _ac_get_default_response();
  $response['_module'] = $module;
  $response['_op'] = $op;
  $func($response, $args);
  
  // Allow modules to alter the response
  drupal_alter('js_response', $response, $module, $op);     
  
  // Output
  _ac_output($response);
}

/**
 * Debug a variable.
 * 
 * This function outputs PHP data to the Firebug console which
 * is useful for quickly debugging XMLHttpRequests.  
 * 
 * @param mixed $var
 * 
 * @param mixed ...
 */
function ac_debug($var) {  
  $args = func_get_args();
  $js  = "console.group('Debug');";
  foreach((array) $args AS $arg){
    $js .= 'console.log(' . drupal_to_js($arg) . ');';
  }
  $js .= "console.groupEnd('Debug');";
      
  drupal_json(array('debug' => $js));
  exit();
}

/**
 * Build callback output.
 * 
 * @param array $response
 * 
 * @see ac_process_request()
 */
function _ac_output($response) {
  $output = array();
  
  // Assign error message when an error has occurred
  if ($response['status'] == AC_STATUS_ERROR){ 
    if (empty($response['message'])){
      $response['message'] = AC_STATUS_ERROR_MESSAGE;
    }
  }
  
  // Add response to output
  // @todo remove arbitrary members
  $output = $response;
  
  // Format
  // Currently only supporting JSON
  $json = drupal_to_js($output);
  drupal_set_header('Content-Type: text/javascript; charset=utf-8'); 
 
  // Gzip output
  if (AC_ENCODING_MAY_COMPRESS && $_SERVER['HTTP_ACCEPT_ENCODING'] == 'gzip,deflate'){
    if ($response['encoding'] == AC_ENCODING_GZIP || ($response['encoding'] == AC_ENCODING_AUTO && _ac_check_length($json))){ 
      if (function_exists('gzencode')){
        drupal_set_header('Content-Encoding: gzip');
        $json = gzencode($json);
      }  
    }                                      
  }
  
  // Set headers passed
  foreach((array) $response['headers'] AS $header){
    drupal_set_header($header);
  }
  
  // Output content
  drupal_set_header('Content-Length: ' . strlen($json));
  echo $json;
  exit();
}        

/**
 * Validate the AC token passed.
 * 
 * @param string $token
 * 
 * @return bool
 */
function _ac_check_token($token){
  return drupal_valid_token($token, 'ac', TRUE);
}

/**
 * Validate weither or not output should be 
 * compressed based on the length of output.
 * 
 * @param string $string
 * 
 * @return bool
 */
function _ac_check_length($string){
  if (strlen($string) >= AC_ENCODING_AUTO_THRESHOLD){
    return TRUE;
  }
  
  return FALSE;
}

/**
 * Get default response structure.
 * 
 * @return array
 */
function _ac_get_default_response() {
  // @todo allow usage of message_func = 'append' etc
  // @todo add optional lock_message_duration
  // @todo add optional lock_message_wrapper
  // @todo add optional lock_message_func
  // @todo add specific classes to all of these messages for overrides specific to AC
  return array(
      // Request status
      'status' => AC_STATUS_OK,   
      // Message wrapped with t().
      // This member is context sensative and will display an error
      // when status is AC_STATUS_ERROR. When no error message is supplied
      // variable_get('ac_error_message') is used.
      'message' => '',                          
      // CSS Selector used to place messages
      'message_wrapper' => '#ac-messages', 
      // Message display duration in milliseconds 
      'message_duration' => 3000, 
      // Message positions allow you to choose where to display the message(s) 
      // relative to the message_wrapper using jQuery traversal methods. 
      // Ex: '.parent().after(message)' becomes evaluated as '$(wrapper).parent().after(message);'
      'message_position' => NULL, 
      // Class(es) for the message  
      'message_class' => '',
      // Additional output, markup, array, object, etc
      'output' => NULL,         
      // Output encoding such as gzip or no plain-text
      'encoding' => AC_ENCODING_AUTO, 
      // Output format. Currently supports JSON only, if you
      // are in need of elaborate output formats view the server module
      'format' => AC_OUTPUT_JSON,  
      // Lock request for a set duration of time in seconds. This optional member prevents
      // users from arbitrarily (or accidentally) issueing the same request several times.
      // Note that this will ONLY work when using AC.checkResponse().
      'lock' => 0, 
      // Optional lock message which is displayed when the user IS locked from a specific
      // request. Currently 'message_wrapper' and 'message_duration' are used just like the other mesages.
      'lock_message' => '',
      // Additional response headers. 
      'headers' => array(),   
    );
}
              
 
